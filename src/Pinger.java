import org.apache.commons.cli.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.*;


class PingerServer {
    private int port;
    private boolean stop;
    private final int TIME_LENGTH = 64;
    private final int SEQ_LENGTH = 32;
    private final int PACKET_LENGTH = TIME_LENGTH + SEQ_LENGTH;
    private DatagramSocket server;

    private class ShutdownHandler extends Thread {
        ShutdownHandler() {
            super("ShutdownHandler");
        }

        public void run() {
            System.out.println("Server is shutdown.");
            stop = true;
        }
    }

    PingerServer(int port) {
        this.port = port;
        this.stop = false;
        Runtime.getRuntime().addShutdownHook(new ShutdownHandler());
    }

    void run() {
        try {
            server = new DatagramSocket(port);
            byte[] data = new byte[PACKET_LENGTH];
            DatagramPacket packet = new DatagramPacket(data, data.length);

            System.out.println(String.format("Pinger server started at %d", port));

            while (!stop) {
                server.receive(packet);
                int port = packet.getPort();
                InetAddress address = packet.getAddress();
                packet = new DatagramPacket(data, data.length, address, port);
                server.send(packet);

                if (packet.getLength() != PACKET_LENGTH) {
                    System.out.println("Broken packet founded (INVALID_PACKET_LENGTH)");
                    continue;
                }

                String info = new String(data, 0, packet.getLength());

                int seq;
                long time;
                try {
                    // FIXME: Here we should use unsigned numbers
                    seq = Integer.parseInt(info.substring(0, SEQ_LENGTH));
                    time = Long.parseLong(info.substring(SEQ_LENGTH, PACKET_LENGTH));
                } catch (NumberFormatException e) {
                    System.out.println("Broken packet founded (INVALID_PACKET_CONTENT)");
                    continue;
                }

                long currentTime = System.currentTimeMillis();

                System.out.println(String.format("%d byte(s) from %s:%d: seq=%d, time=(abs=%d, rel=%d)",
                        packet.getLength(), address.getCanonicalHostName(),
                        port, seq, currentTime, currentTime - time));
            }
            server.close();
        } catch (IOException e) {
            System.err.println("Exception Occured: " + e.getMessage());
            e.printStackTrace();
        }

    }
}

class PingerClient {
    private int port;
    private String host;
    private int packetNumbers;

    private final int TIME_LENGTH = 64;
    private final int SEQ_LENGTH = 32;
    private final int PACKET_LENGTH = TIME_LENGTH + SEQ_LENGTH;

    PingerClient(String host, int port, int counts) {
        this.port = port;
        this.host = host;
        this.packetNumbers = counts;
    }

    void run() {
        try {

            System.out.println(String.format("PING %s:%d: %d data bytes", host, port, PACKET_LENGTH));
            for (int i = 0; i < packetNumbers; i++) {
                DatagramSocket socket = new DatagramSocket();
                ExecutorService executor = Executors.newSingleThreadExecutor();
                InetAddress address = InetAddress.getByName(host);

                long startTime = System.currentTimeMillis();
                byte[] data = String.format("%032d%064d", i + 1, startTime).getBytes();
                DatagramPacket packet = new DatagramPacket(data, data.length, address, port);
                socket.send(packet);
                Future<Object> future = executor.submit(() -> {
                    socket.receive(packet);
                    return null;
                });
                try (socket) {
                    future.get(1, TimeUnit.SECONDS);
                    long currentTime = System.currentTimeMillis();

                    String reply = new String(data, 0, packet.getLength());

                    if (packet.getLength() != PACKET_LENGTH) {
                        System.out.println(String.format(
                                "Broken packet founded for seq %d (INVALID_PACKET_LENGTH)", i + 1));
                        continue;
                    }

                    int seq;
                    long time;
                    try {
                        // FIXME: Here we should use unsigned numbers
                        seq = Integer.parseInt(reply.substring(0, SEQ_LENGTH));
                        time = Long.parseLong(reply.substring(SEQ_LENGTH, PACKET_LENGTH));
                    } catch (NumberFormatException e) {
                        System.out.println(String.format(
                                "Broken packet founded for seq %d (INVALID_PACKET_CONTENT)", i + 1));
                        continue;
                    }

                    assert time == startTime;

                    System.out.println(String.format("%d byte(s) from %s:%d seq=%d rtt=%d ms",
                            packet.getLength(), packet.getAddress().toString(),
                            packet.getPort(), seq, currentTime - startTime));
                    Thread.sleep(Math.max(1000 - (System.currentTimeMillis() - startTime), 0));
                } catch (TimeoutException e) {
                    System.out.println(String.format("Request timeout for seq %d", i + 1));
                    future.cancel(true);
                } catch (InterruptedException e) {
                    System.out.println(String.format("Request cancelled by user for seq %d", i + 1));
                    future.cancel(true);
                } catch (ExecutionException e) {
                    System.out.println(String.format("Failed to request for seq %d", i + 1));
                    future.cancel(true);
                } finally {
                    future.cancel(true);
                    executor.shutdown();
                }
            }
        } catch (IOException e) {
            System.err.println("Exception Occured: " + e.getMessage());
            e.printStackTrace();
        }
    }
}

public class Pinger {
    public static void main(String args[]) {
        Options options = new Options();
        options.addOption("l", "local-port", true, "The port on which Pinger waits " +
                "for packets to be returned.");
        options.addOption("h", "remote-host", true, "The hostname or IP address of a " +
                "remote host.");
        options.addOption("r", "remote-port", true, "The port on which the remote " +
                "host is waiting to receive packets.");
        options.addOption("c", "count", true, "The number of packets to be sent.");
        options.addOption("t", "timeout",false, "Set timeout for a request.");
        options.addOption("u", "usage",false, "To print this usage message.");
        HelpFormatter formatter = new HelpFormatter();
        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(options, args);
            if (cmd.hasOption("usage")) {
                formatter.printHelp("Pinger [-l <arg> -h <arg> -r <arg> -c <arg>|-l <arg>|-u] [-t <arg>]",
                        "Test connection between a pair of hosts using sockets through UDP packets",
                        options, "");
            } else {
                int localPort;
                if (!cmd.hasOption("local-port")) {
                    throw new ParseException("Local port is needed for both client and server mode.");
                }
                try {
                    localPort = Integer.parseInt(cmd.getOptionValue("local-port"));
                } catch (NumberFormatException e) {
                    throw new ParseException("Local port must be an parsable integer");
                }
                if (cmd.hasOption("remote-host") && cmd.hasOption("remote-port") && cmd.hasOption("count")) {
                    String remoteHost = cmd.getOptionValue("remote-host");
                    int remotePort, count;
                    try {
                        remotePort = Integer.parseInt(cmd.getOptionValue("remote-port"));
                    } catch (NumberFormatException e) {
                        throw new ParseException("Remote port must be an parsable integer");
                    }
                    try {
                        count = Integer.parseInt(cmd.getOptionValue("count"));
                    } catch (NumberFormatException e) {
                        throw new ParseException("Count must be an parsable integer");
                    }
                    new PingerClient(remoteHost, remotePort, count).run();
                } else if (cmd.hasOption("remote-host")) {
                    throw new ParseException("Remote host is additional arguments in server mode.");
                } else if (cmd.hasOption("remote-port")) {
                    throw new ParseException("Remote port is additional arguments in server mode.");
                } else if (cmd.hasOption("count")) {
                    throw new ParseException("Count is additional arguments in server mode.");
                } else if (cmd.hasOption("timeout")) {
                    throw new ParseException("Timeout is additional arguments in server mode.");
                } else {
                    new PingerServer(localPort).run();
                }
            }
        } catch (ParseException e) {
            formatter.printHelp("Pinger [-l <arg> -h <arg> -r <arg> -c <arg>|-l <arg>|-u]",
                    "Test connection between a pair of hosts using sockets through UDP packets",
                    options, "\n" + e.getMessage());
        }
    }
}
