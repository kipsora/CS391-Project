package edu.wisc.cs.sdn.sr;

import edu.wisc.cs.sdn.sr.vns.VNSComm;
import net.floodlightcontroller.packet.*;
import net.floodlightcontroller.util.MACAddress;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static net.floodlightcontroller.packet.IPv4.PROTOCOL_ICMP;
import static net.floodlightcontroller.packet.IPv4.PROTOCOL_TCP;
import static net.floodlightcontroller.packet.IPv4.PROTOCOL_UDP;

/**
 * @author Aaron Gember-Jacobson
 */
public class Router {
    /**
     * User under which the router is running
     */
    private String user;

    /**
     * Hostname for the router
     */
    private String host;

    /**
     * Template name for the router; null if no template
     */
    private String template;

    /**
     * Topology ID for the router
     */
    private short topo;

    /**
     * List of the router's interfaces; maps interface name's to interfaces
     */
    private Map<String, Iface> interfaces;

    /**
     * Routing table for the router
     */
    private RouteTable routeTable;

    /**
     * ARP cache for the router
     */
    private ArpCache arpCache;

    /**
     * PCAP dump file for logging all packets sent/received by the router;
     * null if packets should not be logged
     */
    private DumpFile logfile;

    /**
     * Virtual Network Simulator communication manager for the router
     */
    private VNSComm vnsComm;

    /**
     * RIP subsystem
     */
    private RIP rip;

    /**
     * Creates a router for a specific topology, host, and user.
     *
     * @param topo     topology ID for the router
     * @param host     hostname for the router
     * @param user     user under which the router is running
     * @param template template name for the router; null if no template
     */
    public Router(short topo, String host, String user, String template) {
        this.topo = topo;
        this.host = host;
        this.setUser(user);
        this.template = template;
        this.logfile = null;
        this.interfaces = new HashMap<String, Iface>();
        this.routeTable = new RouteTable();
        this.arpCache = new ArpCache(this);
        this.vnsComm = null;
        this.rip = new RIP(this);
    }

    public void init() {
        this.rip.init();
    }

    /**
     * @return PCAP dump file for logging all packets sent/received by the
     * router; null if packets should not be logged
     */
    public DumpFile getLogFile() {
        return this.logfile;
    }

    /**
     * @param logfile PCAP dump file for logging all packets sent/received by
     *                the router; null if packets should not be logged
     */
    public void setLogFile(DumpFile logfile) {
        this.logfile = logfile;
    }

    /**
     * @return template template name for the router; null if no template
     */
    public String getTemplate() {
        return this.template;
    }

    /**
     * @param template template name for the router; null if no template
     */
    public void setTemplate(String template) {
        this.template = template;
    }

    /**
     * @return user under which the router is running
     */
    public String getUser() {
        return this.user;
    }

    /**
     * @param user user under which the router is running; if null, use current
     *             system user
     */
    public void setUser(String user) {
        if (null == user) {
            this.user = System.getProperty("user.name");
        } else {
            this.user = user;
        }
    }

    /**
     * @return hostname for the router
     */
    public String getHost() {
        return this.host;
    }

    /**
     * @return topology ID for the router
     */
    public short getTopo() {
        return this.topo;
    }

    /**
     * @return routing table for the router
     */
    public RouteTable getRouteTable() {
        return this.routeTable;
    }

    /**
     * @return list of the router's interfaces; maps interface name's to
     * interfaces
     */
    public Map<String, Iface> getInterfaces() {
        return this.interfaces;
    }

    /**
     * @param vnsComm Virtual Network System communication manager for the router
     */
    public void setVNSComm(VNSComm vnsComm) {
        this.vnsComm = vnsComm;
    }

    /**
     * Close the PCAP dump file for the router, if logging is enabled.
     */
    public void destroy() {
        if (logfile != null) {
            this.logfile.close();
        }
    }

    /**
     * Load a new routing table from a file.
     *
     * @param routeTableFile the name of the file containing the routing table
     */
    public void loadRouteTable(String routeTableFile) {
        if (!routeTable.load(routeTableFile)) {
            System.err.println("Error setting up routing table from file "
                    + routeTableFile);
            System.exit(1);
        }

        System.out.println("Loading routing table");
        System.out.println("---------------------------------------------");
        System.out.print(this.routeTable.toString());
        System.out.println("---------------------------------------------");
    }

    /**
     * Add an interface to the router.
     *
     * @param ifaceName the name of the interface
     */
    public Iface addInterface(String ifaceName) {
        Iface iface = new Iface(ifaceName);
        this.interfaces.put(ifaceName, iface);
        return iface;
    }

    /**
     * Gets an interface on the router by the interface's name.
     *
     * @param ifaceName name of the desired interface
     * @return requested interface; null if no interface with the given name
     * exists
     */
    public Iface getInterface(String ifaceName) {
        return this.interfaces.get(ifaceName);
    }

    /**
     * Send an Ethernet packet out a specific interface.
     *
     * @param etherPacket an Ethernet packet with all fields, encapsulated
     *                    headers, and payloads completed
     * @param iface       interface on which to send the packet
     * @return true if the packet was sent successfully, otherwise false
     */
    public boolean sendPacket(Ethernet etherPacket, Iface iface) {
        return this.vnsComm.sendPacket(etherPacket, iface.getName());
    }

    /**
     * Handle an Ethernet packet received on a specific interface.
     *
     * @param etherPacket the Ethernet packet that was received
     * @param inIface     the interface on which the packet was received
     */
    public void handlePacket(Ethernet etherPacket, Iface inIface) {
        System.out.println("*** *** -> Received packet: " +
                etherPacket.toString().replace("\n", "\n\t"));

        /********************************************************************/
        /* TODO: Handle packets                                             */
        handleArpPacket(etherPacket, inIface);
        handleIPPacket(etherPacket, inIface);
        /********************************************************************/
    }

    public void handleIPPacket(Ethernet etherPacket, Iface inIface) {
        if (etherPacket.getEtherType() != Ethernet.TYPE_IPv4) {
            return;
        }

        IPv4 ipPacket = (IPv4) etherPacket.getPayload();
        short originalChecksum = ipPacket.getChecksum();
        ipPacket.resetChecksum();
        ipPacket.serialize();
        if (originalChecksum != ipPacket.getChecksum()) {
            System.out.println("######## Broken IP packet: Checksum not matched");
            return;
        }
        // FIXME: Here we reset the checksum and we should recalculate it
        ipPacket.resetChecksum();
        System.out.println("************************** HANDLE IP ********************** TTL: " + String.valueOf(ipPacket.getTtl()));

        boolean isDestinationRouter = false;
        for (Map.Entry<String, Iface> interfaceEntry : getInterfaces()
                .entrySet()) {
            if (ipPacket.getDestinationAddress() == interfaceEntry.getValue().getIpAddress()) {
                isDestinationRouter = true;
                break;
            }
        }
        if (isDestinationRouter || ipPacket.getDestinationAddress() == 0xE0000009) {
            byte ipProtocol = ipPacket.getProtocol();
            switch (ipProtocol) {
                case PROTOCOL_ICMP:
                    ICMP icmpPacket = (ICMP) ipPacket.getPayload();
                    short originalICMPChecksum = icmpPacket.getChecksum();
                    icmpPacket.resetChecksum();
                    icmpPacket.serialize();
                    if (originalICMPChecksum != icmpPacket.getChecksum()) {
                        System.out.println("icmp checksums do not match");
                        return;
                    }
                    sendICMPReply(ICMPType.ECHO_REPLY, etherPacket, inIface);
                    break;
                case PROTOCOL_UDP:
                    UDP udpPacket = (UDP) ipPacket.getPayload();
                    int port = udpPacket.getDestinationPort();

                    if (port != (short) 520) {
                        sendICMPReply(ICMPType.UNREACHABLE_PORT, etherPacket, inIface);
                    } else {
                        rip.handlePacket(etherPacket, inIface);
                    }
                    break;
                case PROTOCOL_TCP:
                    sendICMPReply(ICMPType.UNREACHABLE_PORT, etherPacket, inIface);
                    break;
            }
        }

        ipPacket.setTtl((byte) (ipPacket.getTtl() - 1));
        if (ipPacket.getTtl() == 0) {
            System.out.println("######## TTL reduced to zero, causing TLE ICMP message");
            sendICMPReply(ICMPType.TIME_LIMIT_EXCEED, etherPacket, inIface);
            return;
        }

        RouteTableEntry routeTableEntry = routeTable.lookup(ipPacket.getDestinationAddress());
        if (routeTableEntry == null) {
            System.out.println("######## IP address not found:" + String.valueOf(ipPacket.getDestinationAddress()));
            sendICMPReply(ICMPType.UNREACHABLE_NET, etherPacket, inIface);
            return;
        }
        etherPacket.setSourceMACAddress(interfaces.get(routeTableEntry.getInterface()).getMacAddress().toBytes());

        // FIXME: Here we recalculate the checksum
        ipPacket.serialize();

        int nextHopIP = routeTableEntry.getGatewayAddress();
        if (nextHopIP == 0) {
            nextHopIP = routeTableEntry.getDestinationAddress();
        }

        Iface outIface = interfaces.get(routeTableEntry.getInterface());
        ArpEntry arpEntry = arpCache.lookup(nextHopIP);
        if (arpEntry == null) {
            arpCache.waitForArp(etherPacket, outIface, nextHopIP);
            return;
        }

        etherPacket.setDestinationMACAddress(arpEntry.getMac().toBytes());
        sendPacket(etherPacket, outIface);
    }

    /**
     * Handle an ARP packet received on a specific interface.
     *
     * @param etherPacket the complete ARP packet that was received
     * @param inIface     the interface on which the packet was received
     */
    private void handleArpPacket(Ethernet etherPacket, Iface inIface) {
        // Make sure it's an ARP packet
        if (etherPacket.getEtherType() != Ethernet.TYPE_ARP) {
            return;
        }

        // Get ARP header
        ARP arpPacket = (ARP) etherPacket.getPayload();
        int targetIp = ByteBuffer.wrap(arpPacket.getTargetProtocolAddress()).getInt();

        switch (arpPacket.getOpCode()) {
            case ARP.OP_REQUEST:
                // Check if request is for one of my interfaces
                if (targetIp == inIface.getIpAddress()) {
                    System.out.println("Equals to interface ip sended reply!!!");
                    this.arpCache.sendArpReply(etherPacket, inIface);
                }
                break;
            case ARP.OP_REPLY:
                System.out.println("It is an reply message!!!");
                // Check if reply is for one of my interfaces
                if (targetIp != inIface.getIpAddress()) {
                    break;
                }

                // Update ARP cache with contents of ARP reply
                int senderIp = ByteBuffer.wrap(arpPacket.getSenderProtocolAddress()).getInt();
                ArpRequest request = this.arpCache.insert(new MACAddress(arpPacket.getSenderHardwareAddress()), senderIp);

                // Process pending ARP request entry, if there is one
                if (request != null) {
                    for (Ethernet packet : request.getWaitingPackets()) {
                        /*********************************************************/
                        /* TODO: send packet waiting on this request             */
                        packet.setDestinationMACAddress(arpPacket.getSenderHardwareAddress());
                        sendPacket(packet, request.getIface());
                        /*********************************************************/
                    }
                }
                break;
        }
    }

    private enum ICMPType {
        TIME_LIMIT_EXCEED,
        UNREACHABLE_NET,
        UNREACHABLE_PORT,
        UNREACHABLE_HOST,
        ECHO_REPLY
    }

    public void sendICMPReply(ICMPType type, Ethernet originalPacket, Iface inIface) {
        byte icmpType, icmpCode;
        switch (type) {
            case TIME_LIMIT_EXCEED:
                icmpType = 11;
                icmpCode = 0;
                break;
            case UNREACHABLE_NET:
                icmpType = 3;
                icmpCode = 0;
                break;
            case UNREACHABLE_HOST:
                icmpType = 3;
                icmpCode = 1;
                break;
            case UNREACHABLE_PORT:
                icmpType = 3;
                icmpCode = 3;
                break;
            case ECHO_REPLY:
                icmpType = 0;
                icmpCode = 0;
                break;
            default:
                icmpCode = 0;
                icmpType = 0;
        }

        IPv4 originalIpPacket = (IPv4) originalPacket.getPayload();
        ICMP originalIcmpPacket = null;

        if (type == ICMPType.ECHO_REPLY) {
            originalIcmpPacket = (ICMP) originalIpPacket.getPayload();
        }

        Ethernet etherPacket = new Ethernet();
        etherPacket.setSourceMACAddress(inIface.getMacAddress().toBytes());
        etherPacket.setDestinationMACAddress(originalPacket
                .getSourceMACAddress());
        etherPacket.setEtherType(Ethernet.TYPE_IPv4);

        IPv4 ipPacket = new IPv4();
        ipPacket.setSourceAddress(inIface.getIpAddress());
        ipPacket.setDestinationAddress(originalIpPacket.getSourceAddress());
        ipPacket.setTtl((byte) 64);
        ipPacket.setProtocol(IPv4.PROTOCOL_ICMP);
        ipPacket.setChecksum((short) 0);

        ICMP icmpPacket = new ICMP();
        icmpPacket.setIcmpType(icmpType);
        icmpPacket.setIcmpCode(icmpCode);
        icmpPacket.setChecksum((short) 0);

        if (type == ICMPType.ECHO_REPLY) {
            icmpPacket.setPayload(originalIcmpPacket.getPayload());
            icmpPacket.serialize();
        } else {
            Data dataPacket = new Data();
            ByteBuffer content = ByteBuffer.allocate(32);

            content.putInt(0);
            content.put(originalIpPacket.serialize(), 0, 28);
            dataPacket.setData(content.array());
            icmpPacket.setPayload(dataPacket);
        }

        ipPacket.setPayload(icmpPacket);
        ipPacket.serialize();
        etherPacket.setPayload(ipPacket);

        sendPacket(etherPacket, inIface);

    }
}
