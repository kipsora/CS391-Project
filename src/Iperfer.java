import org.apache.commons.cli.*;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.*;


class IperferServer {
    private int port;
    private boolean stop;
    private final int PACKET_LENGTH = 1024;
    private ServerSocket server;

    private class ShutdownHandler extends Thread {
        ShutdownHandler() {
            super("ShutdownHandler");
        }

        public void run() {
            System.out.println("Server is shutdown.");
            stop = true;
        }
    }

    IperferServer(int port) {
        this.port = port;
        this.stop = false;
        Runtime.getRuntime().addShutdownHook(new ShutdownHandler());
    }

    void run() {
        try {
            System.out.println(String.format("Iperfer server started at %d", port));

            server = new ServerSocket();
            InetSocketAddress host = new InetSocketAddress(port);

            server.bind(host);

            while (!stop) {
                double num = 0;
                byte[] packet = new byte[PACKET_LENGTH];
                double received = 0;
                long start, end;


                Socket client = server.accept();

                start = System.currentTimeMillis();
                while (num > -1) {
                    num = client.getInputStream().read(packet, 0, PACKET_LENGTH);
                    received += num / PACKET_LENGTH;
                }
                end = System.currentTimeMillis();
                client.close();

                double time = (double)(end - start) / 1000.0;
                double speed = (8.0*received/1024.0)/time;
                System.out.println("received=" + (int)(received + 0.01) + " KB rate=" + speed + " Mbps");
            }

            server.close();

        } catch (IOException e) {
            System.err.println("Exception Occured: " + e.getMessage());
            e.printStackTrace();
        }

    }
}

class IperferClient {
    private int port;
    private String host;
    private int time_count;

    private final int PACKET_LENGTH = 1024;

    IperferClient(String host, int port, int time_count) {
        this.port = port;
        this.host = host;
        this.time_count = time_count;
    }

    void run() {
        try {
            Socket client = new Socket();
            InetSocketAddress host = new InetSocketAddress(this.host, this.port);

            client.connect(host);

            double end = System.currentTimeMillis() + time_count * 1000;
            double sent = 0;
            byte [] packet = new byte[PACKET_LENGTH];

            while (System.currentTimeMillis() < end)
            {
                client.getOutputStream().write(packet);
                sent++;
            }
            //closes the connection
            client.close();

            double speed = (8.0*sent/1000.0)/(double)time_count;
            //prints the number of bytes sent and the speed it was sent
            System.out.println("sent=" + (int)(sent + 0.01) + " KB rate=" + speed + " Mbps");
        } catch (IOException e) {
            System.err.println("Exception Occured: " + e.getMessage());
            e.printStackTrace();
        }
    }
}

public class Iperfer {
    public static void main(String args[]) {
        Options options = new Options();
        options.addOption("s", "server", false, "");
        options.addOption("c", "client", false, "");
        options.addOption("h", "host", true, "");
        options.addOption("p", "port", true, "");
        options.addOption("t", "time", true, "");

        HelpFormatter formatter = new HelpFormatter();

        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(options, args);
            int port, time;
            String host;

            if (cmd.hasOption("server")) {
                port = Integer.parseInt(cmd.getOptionValue("port"));

                new IperferServer(port).run();
            } else if (cmd.hasOption("client")) {
                host = cmd.getOptionValue("host");
                port = Integer.parseInt(cmd.getOptionValue("port"));
                time = Integer.parseInt(cmd.getOptionValue("time"));

                new IperferClient(host, port, time).run();
            } else {
                assert false;
            }
        } catch (ParseException e) {
            formatter.printHelp("Pinger [-l <arg> -h <arg> -r <arg> -c <arg>|-l <arg>|-u]",
                    "Test connection between a pair of hosts using sockets through UDP packets",
                    options, "\n" + e.getMessage());
        }
    }
}
